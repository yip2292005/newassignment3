var app = angular.module('clientside', []);

 app.controller('myside', ($scope) => {

    $scope.displayuser = () => {
    var url;
    if($scope.userid == null){
      url = "https://assignment2and3.herokuapp.com/showuser";
    }else{
      url= "https://assignment2and3.herokuapp.com/showuser?id=" + $scope.userid;
    }
        $.ajax({
                url: url ,
                type:'GET',
                success: (result) =>{
                console.log(result);
        $scope.userList = result.list;
        $scope.$apply();
    }
    })

}

 })
 
 app.controller('adduser', function($scope, $rootScope) {
  $rootScope.log = $scope.log;
  $scope.Inserttestdata = () => {
    if ($scope.username1 && $scope.password1 && $scope.name1 && $scope.email1 && $scope.age1 && $scope.company1 && $scope.phone1 && $scope.address1) {
        var jsondata = {
            "username": $scope.username1,
            "password": $scope.password1,
            "name": $scope.name1,
            "email": $scope.email1,
            "age": $scope.age1,
            "company": $scope.company1,
            "phone": $scope.phone1,
            "address": $scope.address1
        };

      $.ajax({
        url: "https://assignment2and3.herokuapp.com/showuser",
        type: 'GET',
        success: (result) => {
          for (i = 0; i < result.list.length; i++) {
            if (result.list[i] != null) {
              if ($scope.username1 == result.list[i].username1) {
                alert("This username has been registered , please use other username!");
                break;
              }
              else {
                if (i == result.list.length-1) {
                  $.ajax({
                      url:"https://assignment2and3.herokuapp.com/Insertuser",
                      type:"POST",
                      data:  JSON.stringify(jsondata),
                      processData: false,
                      contentType: 'application/json',
                      dataType: 'json'
                  });
                  console.log(jsondata);
                }
              }
            }
          }
          $scope.$apply();
        }
      })
    }
    else {
      alert("Please input all data!");
    }
  }
});



app.controller('updateuser', function ($scope,$rootScope) {
  $rootScope.log = $scope.log;
  $scope.update = () => {
        var jsondata = {
            "userid": $scope.userid2,
            "username": $scope.username2,
            "password": $scope.password,
            "name": $scope.name2,
            "email": $scope.email2,
            "age": $scope.age2,
            "company": $scope.company2,
            "phone": $scope.phone2,
            "address": $scope.address2
        };
            if($scope.userid!=null){
                var url = "https://assignment2and3.herokuapp.com/updateuser?newuserID="+ $scope.userid2;
            $.ajax({
            url:url,
            type: "PUT",
            data:  JSON.stringify(jsondata),
            processData: false,
            contentType: 'application/json',
            dataType: 'json'
        })
              alert("sucessful update to database")
            }else{
                alert("Please input the id number");
            }
             
    }
 
})


app.controller('delete', function ($scope,$rootScope) {
    $rootScope.log = log;
    $scope.deletuser = () => {
        if($scope.id != null){
            var url = "https://assignment2and3.herokuapp.com/removeuser?newuserID=" + $scope.id;
                $.ajax({
                    url:url,
                type:"DELETE"
            });
            alert("sucessful delte the user data")
        }
        else {
            alert("Please input the id number to delete the user for database");
        }
    }
});

var log;
app.controller('loginusers', function($scope, $rootScope) {
  $rootScope.log = log;
  $scope.logins = () => {
    $.ajax({
      url: "https://assignment2and3.herokuapp.com/showuser",
      type: 'GET',
      success: (result) => {
        if ($scope.loginuser && $scope.loginpassword) {
          for (i = 0; i < result.list.length; i++) {
            if (result.list[i] != null) {
              if (result.list[i].username == $scope.loginuser) {
  
                if (result.list[i].password == $scope.loginpassword) {
               
                  log = true;
                  $rootScope.log = log;
               
                  $rootScope.loginusers = result.list[i].name;
                  break;
                }
                else{
                        alert('your password wrong');
                        break;
                }
              }
              else {
                if(i == result.list.length){
                        alert("the database cannot has match data");
                        break;
                }
              }
            }
          }
        }
        else {
      alert("please input your username and password");
        }
         $scope.$apply();
      }
    })
  }
});